import numpy as np
from scipy.constants import G # costante gravitazionale
import json
from params import * 


# Modello fisico
def calcola_acc_grav(var_lagrangiane):

    r = np.linalg.norm(var_lagrangiane[:Ndims])

    return r, G*M1/r**2


# Modello di densita' atmosferica: valido dai 180 a 1000 km di altezza
def modello_atmosfera(var_lagrangiane):
    
    # Altezza dal suolo
    h = np.linalg.norm(var_lagrangiane[:Ndims])-raggio_terrestre
    
    m = 27 - 0.012*(h*1e-3-200)    # molecular mass, conversione h in kilometri
    
    F = 80.   #  solar radio flux at 10.7 cm  [10^-22 [watt]/[m]**2*[Hz]]  60-300
    
    Ap = 50   # geomagnetic index natural number  0-400
    
    t = 900 + 2.5*(F-70) + 1.5*Ap        # temperatura della termosfera in kelvin

    return 6e-10*np.exp((175-h*1e-3)*(m/t))    # units: [[kg]/[m]**3]
    

def calcola_modulo_attrito_atmo(var_lagrangiane):
    
    v = np.linalg.norm(var_lagrangiane[Ndims:2*Ndims])

    modulo_attrito_atmo = 0.

    if area is not None:
        modulo_attrito_atmo = modello_atmosfera(var_lagrangiane)*(v**2)*(area)
    #print("atmo: %s, v: %s, area: %s" % (modello_atmosfera(var_lagrangiane), v, area))

    return v, modulo_attrito_atmo/m2


def calcola_accelerazioni(var_lagrangiane, t):

    r, modulo_acc_grav = calcola_acc_grav(var_lagrangiane)
    
    if area != 0:
        
        v, modulo_attrito_atmo = calcola_modulo_attrito_atmo(var_lagrangiane)
        #print(modulo_attrito_atmo)
        a1 = - np.add(modulo_acc_grav * var_lagrangiane[0:Ndims]/r, \
                      modulo_attrito_atmo * var_lagrangiane[Ndims:2*Ndims]/v )
        motore = np.heaviside(t-t_acc,0)*np.heaviside(t_acc+durata-t,0)
        
        a_motore = (acc_motore/m2)*motore*var_lagrangiane[Ndims:2*Ndims]/v
        
        #print(a_motore)
        a2 = np.add(a1,a_motore)
        
        return a2
    
    else:
        
        return -modulo_acc_grav * var_lagrangiane[0:Ndims]/r


################

def condizioni_iniziali():
    # Scelta di coordinate tale che si parte sempre su y,z=0
    pos = np.array([distanza, 0., 0.])
    # Velocita orbitale calcolata analiticamente
    vel = np.array([0., np.sqrt(G*M1/distanza), 0.])

    if area == 0:
        vel = [0.,0.,0.]
    
    var_lagrangiane = np.concatenate((pos,vel))

    return var_lagrangiane


def calcola_energia_cinetica(var_lagrangiane):

    return 0.5*m2*(np.linalg.norm(var_lagrangiane[Ndims:2*Ndims]))**2



def calcola_energia_potenziale(var_lagrangiane):

    return -G*M1*m2/(np.linalg.norm(var_lagrangiane[:Ndims]))



def calcola_energia_meccanica(file):
    
    energia = []
    t = open(file)
    
    for line in t: 
        l = json.loads(line)
        var_lagrangiane = np.array(l)
        #calcolo energia meccanica da ciascun dato poiche ho le vx,vy
        val = calcola_energia_cinetica(var_lagrangiane) + calcola_energia_potenziale(var_lagrangiane)
        
        energia.append(val)
        
    return np.array(energia)
