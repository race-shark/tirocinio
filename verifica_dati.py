import json
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.constants import G
import modello_fisico as md
from params import *



with open("valori_RK4.txt") as f:
    data = []
    
    for line in f:
        data.append(json.loads(line))
        
    data1 = np.array(data)
    x_rk = data1[:,6]
    print(data[50000])
    

with open("valori_verlet.txt") as t:
    
    data2 = []
    
    for line in t :
        data2.append(json.loads(line))
        
    data3 = np.array(data2)
    x_verlet = data3[:,6]
    print(data2[1000000])


y=[]

for i in range(len(x_rk)):
    y.append(np.abs(x_rk[i]-x_verlet[i*20]))
    
plt.plot(np.linspace(1,len(x_rk),len(x_rk))*10000,y)
plt.xlabel("secondi")
plt.ylabel("differenza")
